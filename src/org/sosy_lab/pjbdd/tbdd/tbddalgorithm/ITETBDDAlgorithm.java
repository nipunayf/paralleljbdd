// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddalgorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.tbdd.TBDDNodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;

/** Implementation of the ITE algorithm for TBDDs. */
public class ITETBDDAlgorithm extends AbstractTBDDAlgorithm {

  public ITETBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, TBDDNodeManager tbddNodeManager) {
    super(computedTable, tbddNodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeOp(TBDD f1, TBDD f2, ApplyOp op) {

    switch (op) {
      case OP_OR:
        return makeIte(f1, makeTrue(), f2);
      case OP_AND:
        return makeIte(f1, f2, makeFalse());
      case OP_XOR:
        return makeIte(f1, makeNot(f2), f2);
      case OP_NOR:
        return makeIte(f1, makeFalse(), makeNot(f2));
      case OP_NAND:
        return makeIte(f1, makeNot(f2), makeTrue());
      case OP_IMP:
        return makeOp(makeNot(f1), f2, ApplyOp.OP_OR);
      case OP_XNOR:
        return makeIte(f1, f2, makeNot(f2));
    }
    return makeFalse();
  }

  /** {@inheritDoc} */
  @Override
  public TBDD makeNot(TBDD f) {
    return makeIte(f, makeFalse(), makeTrue());
  }

  @Override
  public TBDD makeIte(TBDD ifTBDD, TBDD thenTBDD, TBDD elseTBDD) {
    return terminalIteCheck(ifTBDD, thenTBDD, elseTBDD)
        .orElseGet(
            () -> {

              // Calculating topVar based on tags, if all tags are the same, use variable
              // instead and add a tag later.
              int tempTopVar =
                  topVar(
                      level(ifTBDD.getTag()), level(thenTBDD.getTag()), level(elseTBDD.getTag()));
              int topVar;
              if ((ifTBDD.getTag() == thenTBDD.getTag() && ifTBDD.getTag() == elseTBDD.getTag())
                  && thenTBDD.getTag() == elseTBDD.getTag()) {
                System.out.println("Equal tags detected");
                topVar = topVar(level(ifTBDD), level(thenTBDD), level(elseTBDD));
              } else {
                topVar = tempTopVar;
              }
              int next = getNextVar(topVar);

              TBDD low =
                  makeIte(
                      low(ifTBDD, topVar, next),
                      low(thenTBDD, topVar, next),
                      low(elseTBDD, topVar, next));
              TBDD high =
                  makeIte(high(ifTBDD, topVar), high(thenTBDD, topVar), high(elseTBDD, topVar));
              TBDD res = makeNode(low, high, topVar, next);
              if (tempTopVar != topVar) {
                res = makeNode(res, tbddNodeManager.getFalse(), tempTopVar, topVar);
              }
              cacheItem(ifTBDD, thenTBDD, elseTBDD, res);
              return res;
            });
  }

  /**
   * Chain base-case check and cache check.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of (base-case/cache) hit or empty
   */
  protected Optional<TBDD> terminalIteCheck(TBDD f1, TBDD f2, TBDD f3) {
    return checkTerminalCases(f1, f2, f3).or(() -> checkITECache(f1, f2, f3));
  }

  /**
   * check if 'ITE' input triple matches terminal case.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of matched case or Optional empty
   */
  protected Optional<TBDD> checkTerminalCases(TBDD f1, TBDD f2, TBDD f3) {
    if (f1.equals(makeTrue())) {
      return Optional.of(f2);
    }
    if (f1.equals(makeFalse())) {
      return Optional.of(f3);
    }
    if (f2.equals(f3)) {
      return Optional.of(f2);
    }
    return Optional.empty();
  }

  @Override
  public int getCacheSize() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getCacheNodeCount() {
    // TODO Auto-generated method stub
    return 0;
  }
}

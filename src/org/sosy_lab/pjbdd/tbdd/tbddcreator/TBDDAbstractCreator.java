// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd.tbddcreator;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.sosy_lab.pjbdd.api.AbstractCreatorBuilder;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.bdd.BDDCreator;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;

/**
 * {@link AbstractCreatorBuilder} implementation with {@link TBDD} objects as TBDD data structure.
 *
 * @author Stephan Holzner, Simon Raths
 * @see Creator
 * @since tba
 */
public abstract class TBDDAbstractCreator {

  /** lock object for reorder operations: read lock for normal operations and write for reorder. */
  protected final ReadWriteLock reorderLock;

  protected final NodeManager<TBDD> nodeManager;

  protected final SatAlgorithm<TBDD> satAlgorithm;

  /**
   * Creates new {@link BDDCreator} instances with given parameters.
   *
   * @param nodeManager - the node manager used
   * @param satAlgorithm - the sat algorithm used
   */
  protected TBDDAbstractCreator(NodeManager<TBDD> nodeManager, SatAlgorithm<TBDD> satAlgorithm) {
    this.reorderLock = new ReentrantReadWriteLock();
    this.nodeManager = nodeManager;
    this.satAlgorithm = satAlgorithm;
  }

  /** shutdown creator. */
  public void shutDown() {
    nodeManager.shutdown();
  }

  /**
   * set new variable order.
   *
   * @param pOrder - the new order
   */
  public void setVarOrder(List<Integer> pOrder) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarOrder(pOrder);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /**
   * get current variable count.
   *
   * @return current variable count
   */
  public int getVariableCount() {
    return nodeManager.getVarCount();
  }

  /**
   * get current variable order.
   *
   * @return current variable order.
   */
  public int[] getVariableOrdering() {
    return nodeManager.getCurrentOrdering();
  }

  /**
   * set new variable count.
   *
   * @param count - the new count.
   */
  public void setVariableCount(int count) {
    reorderLock.readLock().lock();
    try {
      nodeManager.setVarCount(count);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
}

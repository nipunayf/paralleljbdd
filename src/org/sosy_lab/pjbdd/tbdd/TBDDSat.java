// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.tbdd;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;

public class TBDDSat implements SatAlgorithm<TBDD> {

  protected final Cache<TBDD, BigInteger> satCountCache;

  protected final TBDDNodeManager nodeManager;

  public TBDDSat(Cache<TBDD, BigInteger> satCountCache, TBDDNodeManager nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  @Override
  public TBDD anySat(TBDD tbdd) {
    if (tbdd.isLeaf()) {
      return tbdd;
    }
    if (tbdd.getLow().isFalse()) {
      return nodeManager.makeNode(
          nodeManager.getFalse(), anySat(tbdd.getHigh()), tbdd.getVariable());
    } else {
      return nodeManager.makeNode(
          anySat(tbdd.getLow()), nodeManager.getFalse(), tbdd.getVariable());
    }
  }

  @Override
  public BigInteger satCount(TBDD b) {
    int preRootTag = level(b) - (level(b) - level(b.getTag()));
    return BigInteger.valueOf(2).pow(preRootTag).multiply(satCountRec(b));
  }

  protected BigInteger satCountRec(TBDD root) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      return BigInteger.ONE;
    }
    BigInteger count = satCountCache.get(root);
    if (count != null) {
      return count;
    }

    BigInteger s = BigInteger.valueOf(2).pow((levelLowTag(root) - level(root) - 1));
    BigInteger size = s.multiply(satCountRec(root.getLow()));

    s = BigInteger.valueOf(2).pow((levelHighTag(root) - level(root) - 1));
    size = size.add(s.multiply(satCountRec(root.getHigh())));

    satCountCache.put(root, size);

    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param tbdd - the bdd
   * @return level of bdd
   */
  private int level(TBDD tbdd) {
    return nodeManager.level(tbdd.getVariable());
  }

  private int level(int tag) {
    return nodeManager.level(tag);
  }

  private int levelLowTag(TBDD tbdd) {
    int temp = nodeManager.level(tbdd.getLowTag());
    if (temp == -3) {
      return level(nodeManager.getFalse());
    } else {
      return temp;
    }
  }

  private int levelHighTag(TBDD tbdd) {
    int temp = nodeManager.level(tbdd.getHighTag());
    if (temp == -3) {
      return level(nodeManager.getFalse());
    } else {
      return temp;
    }
  }
}

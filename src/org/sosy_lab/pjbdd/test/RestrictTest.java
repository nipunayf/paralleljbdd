// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.api.DD;

public class RestrictTest extends ZDDCombinatorTest {

  @Override
  public void test() {
    DD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    DD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    DD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    DD bdd0 = creator.union(var2, var1);
    DD bdd1 = creator.union(bdd0, var0);

    DD restrict = creator.restrict(creator.empty(), bdd1);
    assertEquals(creator.empty(), restrict);

    DD restrict1 = creator.restrict(bdd1, creator.empty());
    assertEquals(creator.empty(), restrict1);

    DD restrict2 = creator.restrict(bdd1, bdd1);
    assertEquals(bdd1, restrict2);
  }
}

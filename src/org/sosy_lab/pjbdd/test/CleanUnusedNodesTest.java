// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import java.util.Collection;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.Statistics;
import org.sosy_lab.pjbdd.examples.NQueens;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class CleanUnusedNodesTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void test() {
    triggerGarbageCollectionAndTriggerCleanDecrementsNodeCount();
    triggerCleanDoesNotIncreaseTableSize();
    creator.shutDown();
  }

  @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(
      value = "DM_GC",
      justification =
          "This test case checks unique table cleaning after garbage collection. "
              + "Immediate GC is required for this test.")
  private void triggerGarbageCollectionAndTriggerCleanDecrementsNodeCount() {
    NQueens test = new NQueens(5, creator);
    test.build();

    Creator.Stats statsBeforeClean = Statistics.of(creator);
    test = null;
    System.gc();

    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {
      org.junit.Assume.assumeNoException(e);
    }
    creator.cleanUnusedNodes();
    Creator.Stats statsAfterClean = Statistics.of(creator);

    Assert.assertTrue(statsAfterClean.getNodeCount() < statsBeforeClean.getNodeCount());
  }

  private void triggerCleanDoesNotIncreaseTableSize() {
    NQueens test = new NQueens(5, creator);
    test.build();

    Creator.Stats statsBeforeClean = Statistics.of(creator);
    creator.cleanUnusedNodes();
    Creator.Stats statsAfterClean = Statistics.of(creator);
    Assert.assertEquals(
        statsBeforeClean.getUniqueTableSize(), statsAfterClean.getUniqueTableSize());
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.examples.NQueens;

/**
 * Make 'SAT_COUNT' test class uses {@link CreatorCombinatorTest} as base class to perform make
 * 'SAT_COUNT' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class SatCountTest extends CreatorCombinatorTest {

  @Parameters(name = "synchronizeReordering= {0}, ct={2}, table={1}")
  public static Collection<Object[]> getCreatorData() {
    return CreatorCombinatorTest.data();
  }

  @Parameter(0)
  public boolean synchronizeReordering;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  @Parameter(2)
  public CreatorType ct;

  @Override
  protected boolean synchronizeReorderingToUse() {
    return synchronizeReordering;
  }

  @Override
  protected UniqueTableType uniqueTableTypeToUse() {
    return uniqueTableType;
  }

  @Override
  protected CreatorType creatorTypeToUse() {
    return ct;
  }

  @Test
  public void testSATCountQueens() {

    NQueens queens = new NQueens(7, creator);
    queens.build();
    assertEquals(BigInteger.valueOf(40), queens.solve());

    NQueens queens2 = new NQueens(8, creator);
    queens2.build();
    assertEquals(BigInteger.valueOf(92), queens2.solve());

    creator.shutDown();
  }

  @Test
  public void testSATCountSimple() {
    DD a = creator.makeVariable();
    DD b = creator.makeVariable();
    DD c = creator.makeVariable();
    DD ite =
        creator.makeIte(a, b, c);
    assertEquals(BigInteger.valueOf(4), creator.satCount(ite));

    DD iteAnd = creator.makeAnd(creator.makeAnd(a, b), ite);
    assertEquals(BigInteger.valueOf(1), creator.satCount(iteAnd));

    creator.shutDown();
  }

  @Test
  public void testSATCountSimple2() {
    DD a = creator.makeVariable();
    DD b = creator.makeVariable();
    DD c = creator.makeVariable();
    DD d = creator.makeVariable();

    DD aAndB = creator.makeAnd(a, b);
    // Only the and has 1 sat assignment
    assertEquals(BigInteger.valueOf(1), creator.satCount(aAndB));

    DD BAndCAndD = creator.makeAnd(b, creator.makeAnd(d, c));
    assertEquals(BigInteger.valueOf(1), creator.satCount(BAndCAndD));
    DD or = creator.makeOr(aAndB, BAndCAndD);
    // a and b true is sat, c and d irrelevant -> 2^2 = 4
    // b, d, c = true is sat -> 2^1 = 2
    // but the assignment a,b,c,d true is shared between both so -1
    assertEquals(BigInteger.valueOf(5), creator.satCount(or));

    DD e = creator.makeVariable();
    DD f = creator.makeVariable();
    DD eAndF = creator.makeAnd(e, f);
    assertEquals(BigInteger.valueOf(1), creator.satCount(eAndF));
    DD or2 = creator.makeOr(eAndF, or);
    // a and b true is sat, c, d, e, f irrelevant -> 2^4 = 16
    // b, d, c = true is sat -> 2^3 = 8
    // e, f = true -> 2^4 = 16
    // minus shared assignments (-9)
    assertEquals(BigInteger.valueOf(31), creator.satCount(or2));

    creator.shutDown();
  }
}

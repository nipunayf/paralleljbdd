// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

public class MulTest extends ZDDCombinatorTest {

  @Override
  public void test() {
    DD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    DD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    DD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    DD bdd0 = creator.union(var2, var1);
    DD bdd1 = creator.union(bdd0, var0);

    DD mul0 = creator.product(creator.empty(), bdd1);
    Assert.assertEquals(creator.empty(), mul0);

    DD mul1 = creator.product(bdd1, creator.empty());
    Assert.assertEquals(creator.empty(), mul1);

    DD mul2 = creator.product(bdd1, creator.base());
    assertEquals(bdd1, mul2);

    DD mul3 = creator.product(creator.base(), bdd1);
    assertEquals(bdd1, mul3);
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import org.junit.Assert;
import org.junit.Test;

public class BitMaskTupleTest {

  @Test
  public void testBitMasking() {
    int tuple = BitMaskIntTupleUtils.createTuple(100, 122);

    int first = BitMaskIntTupleUtils.getFirst(tuple);
    int second = BitMaskIntTupleUtils.getSecond(tuple);

    Assert.assertEquals(100, first);
    Assert.assertEquals(122, second);

    tuple = BitMaskIntTupleUtils.createTuple(0, 32000);

    first = BitMaskIntTupleUtils.getFirst(tuple);
    second = BitMaskIntTupleUtils.getSecond(tuple);

    Assert.assertEquals(0, first);
    Assert.assertEquals(32000, second);

    tuple = BitMaskIntTupleUtils.createTuple(32001, 0);

    first = BitMaskIntTupleUtils.getFirst(tuple);
    second = BitMaskIntTupleUtils.getSecond(tuple);

    Assert.assertEquals(32001, first);
    Assert.assertEquals(0, second);

    tuple = BitMaskIntTupleUtils.createTuple(Short.MAX_VALUE - 1, Short.MAX_VALUE - 1);

    first = BitMaskIntTupleUtils.getFirst(tuple);
    second = BitMaskIntTupleUtils.getSecond(tuple);

    Assert.assertEquals(Short.MAX_VALUE - 1, first);
    Assert.assertEquals(Short.MAX_VALUE - 1, second);
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd;

import static org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm.ApplyOp.OP_OR;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.ManipulatingAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * An abstract {@link DDAlgorithm} implementation.
 *
 * @author Stephan Holzner
 * @see ManipulatingAlgorithm
 * @see DDAlgorithm
 * @since 1.1
 */
public abstract class AbstractBDDAlgorithm<V extends DD> extends ManipulatingAlgorithm<V>
    implements DDAlgorithm<V> {

  /** additional cache for existential quantification. */
  protected Cache<Integer, Cache.CacheData> quantCache;

  protected Cache<Integer, Cache.CacheData> composeCache;

  public AbstractBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager<V> nodeManager) {
    super(computedTable, nodeManager);
    quantCache = computedTable.cleanCopy();
    composeCache = computedTable.cleanCopy();
  }

  public V makeExist(V f, int level) {
    if (f.isLeaf() || level(f) > level) {
      return f;
    }
    int hash = HashCodeGenerator.generateHashCode(f.hashCode(), level);
    @SuppressWarnings("unchecked")
    Cache.CacheDataExQuant<V> data = (Cache.CacheDataExQuant<V>) quantCache.get(hash);
    if (data != null && data.getF().equals(f) && data.getLevel() == level) {
      return data.getRes();
    }
    V res = null;
    if (level(f) == level) {
      res = makeOp(nodeManager.getHigh(f), nodeManager.getLow(f), OP_OR);
    }
    if (res == null) {
      V high = makeExist(nodeManager.getHigh(f), level);
      V low = makeExist(nodeManager.getLow(f), level);

      res = makeNode(low, high, f.getVariable());
    }

    Cache.CacheDataExQuant<V> d = new Cache.CacheDataExQuant<>();
    d.setF(f);
    d.setLevel(level);
    d.setRes(res);
    quantCache.put(hash, d);
    return res;
  }

  @Override
  public V makeExists(V f, int... levels) {
    if (f.isLeaf() || (level(f) > levels[0] && levels.length == 1)) {
      return f;
    }

    int hash = HashCodeGenerator.generateHashCode(f.hashCode(), levels[0]);
    @SuppressWarnings("unchecked")
    Cache.CacheDataExQuantVararg<V> data = (Cache.CacheDataExQuantVararg<V>) quantCache.get(hash);
    V res = null;

    if (data != null && data.matches(f, levels)) {
      res = data.getRes();
    }
    if (res == null) {
      V fLow = nodeManager.getLow(f);
      V fHigh = nodeManager.getHigh(f);
      int fLevel = level(f);
      if (fLevel > levels[0]) {
        int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
        res = makeExists(f, newTail);
      } else if (fLevel == levels[0]) {
        if (levels.length == 1) {
          res = makeOp(fHigh, fLow, OP_OR);
        } else {
          int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
          res = makeOp(makeExists(fLow, newTail), makeExists(fHigh, newTail), OP_OR);
        }
      } else {
        V high = makeExists(fHigh, levels);
        V low = makeExists(fLow, levels);
        res = makeNode(low, high, f.getVariable());
      }

      Cache.CacheDataExQuantVararg<V> d = new Cache.CacheDataExQuantVararg<>();
      d.setF(f);
      d.setLevels(levels);
      d.setRes(res);
      quantCache.put(hash, d);
    }
    return res;
  }

  @Override
  public V makeCompose(V f1, int var, V f2) {
    if (level(f1) > nodeManager.level(var)) {
      return f1;
    }
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), var, f2.hashCode());
    @SuppressWarnings("unchecked")
    Cache.CacheDataBinaryOp<V> data = (Cache.CacheDataBinaryOp<V>) composeCache.get(hash);
    if (data != null && data.matches(f1, f2, var)) {
      return data.getRes();
    }
    V res;

    V f1Low = nodeManager.getLow(f1);
    V f1High = nodeManager.getHigh(f1);
    if (f1.getVariable() == var) {
      res = makeIte(f2, f1High, f1Low);
    } else {
      V i = makeCompose(f1High, var, f2);
      V e = makeCompose(f1Low, var, f2);
      res = makeIte(nodeManager.makeIthVar(f1.getVariable()), i, e);
    }
    data = new Cache.CacheDataBinaryOp<>();
    data.setRes(res);
    data.setOp(var);
    data.setF1(f1);
    data.setF2(f2);
    composeCache.put(hash, data);
    return res;
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.AbstractBDDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/**
 * A {@link DDAlgorithm} implementation which uses {@link AbstractBDDAlgorithm} as base class.
 * Performs serial 'APPLY' calculations.
 *
 * @author Stephan Holzner
 * @see AbstractBDDAlgorithm
 * @since 1.1
 */
public class ApplyBDDAlgorithm<V extends DD> extends ITEBDDAlgorithm<V> {

  /**
   * Creates new {@link ApplyBDDAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   */
  public ApplyBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager<V> nodeManager) {
    super(computedTable, nodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public V makeNot(V f) {
    return makeNotRecursive(f);
  }

  /**
   * Helper method for recursive 'NOT' calls.
   *
   * @param f - the bdd
   * @return the negation of f
   */
  private V makeNotRecursive(V f) {
    return terminalNotCheck(f)
        .orElseGet(
            () -> {
              V bdd =
                  makeNode(
                      makeNotRecursive(getLow(f)), makeNotRecursive(getHigh(f)), f.getVariable());
              this.cacheUnaryItem(f, bdd);
              return bdd;
            });
  }

  /**
   * Asynchronous calculation for applying an operation on two input arguments, with an given
   * terminal function.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - the operation to be applied
   * @return (f1 op f2)
   */
  @Override
  public V makeOp(V f1, V f2, ApplyOp op) {
    return terminalCheck(f1, f2, op)
        .orElseGet(
            () -> {
              int topVar = topVar(level(f1), level(f2));
              V low = makeOp(low(f1, topVar), low(f2, topVar), op);
              V high = makeOp(high(f1, topVar), high(f2, topVar), op);

              V res = makeNode(low, high, topVar);
              this.cacheBinaryItem(f1, f2, op.ordinal(), res);
              return res;
            });
  }

  /**
   * Apply terminal check resolver.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @param op - the operation to be applied
   * @return optional of terminal check
   */
  protected Optional<V> terminalCheck(V f1, V f2, ApplyOp op) {
    switch (op) {
      case OP_AND:
        return terminalAndCheck(f1, f2);
      case OP_OR:
        return terminalOrCheck(f1, f2);
      case OP_IMP:
        return terminalImplyCheck(f1, f2);
      case OP_XNOR:
        return terminalXnorCheck(f1, f2);
      case OP_NAND:
        return terminalNandCheck(f1, f2);
      case OP_NOR:
        return terminalNorCheck(f1, f2);
      case OP_XOR:
        return terminalXorCheck(f1, f2);
      default:
        throw new IllegalArgumentException("No such op supported yet");
    }
  }

  /**
   * Nested base case and cache check for apply op 'XOR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalXorCheck(V f1, V f2) {
    if (f1.equals(f2)) {
      return Optional.of(makeFalse());
    }
    if (f1.isTrue()) {
      return Optional.of(makeNot(f2));
    }
    if (f2.isTrue()) {
      return Optional.of(makeNot(f1));
    }
    if (f1.isFalse()) {
      return Optional.of(f2);
    }
    if (f2.isFalse()) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_XOR.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'NOR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalNorCheck(V f1, V f2) {
    if (f1.isTrue() || f2.isTrue()) {
      return Optional.of(makeFalse());
    }
    if (f1.isFalse() || f1.equals(f2)) {
      return Optional.of(makeNot(f2));
    }
    if (f2.isFalse()) {
      return Optional.of(makeNot(f1));
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_NOR.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'XNOR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalXnorCheck(V f1, V f2) {
    if (f1.equals(f2)) {
      return Optional.of(makeTrue());
    }
    if (f1.isFalse()) {
      return Optional.of(makeNot(f2));
    }
    if (f2.isFalse()) {
      return Optional.of(makeNot(f1));
    }
    if (f2.isTrue()) {
      return Optional.of(f1);
    }
    if (f1.isTrue()) {
      return Optional.of(f2);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_XNOR.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'NAND' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalNandCheck(V f1, V f2) {

    if (f1.isFalse() || f2.isFalse()) {
      return Optional.of(makeTrue());
    }
    if (f1.isTrue() && f2.isTrue()) {
      return Optional.of(makeFalse());
    }
    if (f1.equals(f2) || f2.isTrue()) {
      return Optional.of(makeNot(f1));
    }
    if (f1.isTrue()) {
      return Optional.of(makeNot(f2));
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_NAND.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'AND' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalAndCheck(V f1, V f2) {
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    if (f1.isFalse() || f2.isFalse()) {
      return Optional.of(makeFalse());
    }
    if (f1.isTrue()) {
      return Optional.of(f2);
    }
    if (f2.isTrue()) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_AND.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'OR' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalOrCheck(V f1, V f2) {
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    if (f1.isTrue() || f2.isTrue()) {
      return Optional.of(makeTrue());
    }
    if (f2.isFalse()) {
      return Optional.of(f1);
    }
    if (f1.isFalse()) {
      return Optional.of(f2);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_OR.ordinal());
  }

  /**
   * Nested base case and cache check for the negation of an argument.
   *
   * @param f - the {@link DD} argument
   * @return optional of check chain
   */
  protected Optional<V> terminalNotCheck(V f) {
    if (f.isTrue()) {
      return Optional.of(makeFalse());
    }
    if (f.isFalse()) {
      return Optional.of(makeTrue());
    }
    return checkNotCache(f);
  }

  /**
   * Nested base case and cache check for apply op 'IMPLY' on two arguments.
   *
   * @param f1 - getIf {@link DD} argument
   * @param f2 - getThen {@link DD} argument
   * @return optional of check chain
   */
  private Optional<V> terminalImplyCheck(V f1, V f2) {
    if (f1.equals(f2)) {
      return Optional.of(makeTrue());
    }
    if (f1.isFalse() || f2.isTrue()) {
      return Optional.of(makeTrue());
    }
    if (f1.isTrue()) {
      return Optional.of(f2);
    }
    if (f2.isFalse()) {
      return Optional.of(makeNot(f1));
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_IMP.ordinal());
  }

  // ITE
  @Override
  protected Optional<V> checkTerminalCases(V f1, V f2, V f3) {
    if (f2.equals(makeFalse()) && f3.equals(makeTrue())) {
      return Optional.of(makeNot(f1));
    }

    if (f2.isTrue()) {
      return Optional.of(makeOp(f1, f3, ApplyOp.OP_OR));
    }

    if (f2.isFalse()) {
      return Optional.of(makeOp(makeNot(f1), f3, ApplyOp.OP_AND));
    }

    if (f3.isTrue()) {
      return Optional.of(makeOp(makeNot(f1), f2, ApplyOp.OP_OR));
    }

    if (f3.isFalse()) {
      return Optional.of(makeOp(f1, f2, ApplyOp.OP_AND));
    }
    return super.checkTerminalCases(f1, f2, f3);
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.node.ReductionRule;

/**
 * {@link ReductionRule} implementation for binary decision diagram types specific reduction rules.
 *
 * @author Stephan Holzner
 * @see org.sosy_lab.pjbdd.core.node.ReductionRule
 * @since 1.2
 */
public final class BDDReductionRule<V extends DD> implements ReductionRule<V> {

  /** {@inheritDoc} */
  @Override
  public Optional<V> apply(V low, V high, int var) {
    return low.equals(high) ? Optional.of(low) : Optional.empty();
  }
}

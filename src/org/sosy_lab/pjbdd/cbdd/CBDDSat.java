// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>, Sebastian Niedner
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.cbdd;

import com.google.common.base.Preconditions;
import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.BitMaskIntTupleUtils;

/**
 * CBDD based {@link SatAlgorithm} implementation. Uses serial algorithms for cbdd sat operations.
 *
 * @author Sebastian Niedner
 * @see SatAlgorithm
 * @since ?.?
 */
public class CBDDSat implements SatAlgorithm<DD> {

  protected final Cache<DD, BigInteger> satCountCache;

  protected final NodeManager<DD> nodeManager;

  public CBDDSat(Cache<DD, BigInteger> satCountCache, NodeManager<DD> nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(DD b) {
    return BigInteger.valueOf(2).pow(level(b)).multiply(satCountRec(b));
  }

  /**
   * recursive calculation of number of possible satisfying truth assignments for given bdd.
   *
   * @param root - a bdd
   * @return root's number of possible satisfying truth assignments
   */
  protected BigInteger satCountRec(DD root) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      return BigInteger.ONE;
    }
    BigInteger count = satCountCache.get(root);
    if (count != null) {
      return count;
    }

    BigInteger s = BigInteger.valueOf(2).pow(level(root.getLow()) - chainEndLevel(root) - 1);
    BigInteger size = s.multiply(satCountRec(nodeManager.getLow(root)));

    s = selfValue(level(root), chainEndLevel(root), level(root.getHigh()));
    size = size.add(s.multiply(satCountRec(nodeManager.getHigh(root))));

    satCountCache.put(root, size);

    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param bdd - the bdd
   * @return level of bdd
   */
  private int level(DD bdd) {
    return nodeManager.level(bdd.getVariable());
  }

  private int chainEndLevel(DD bdd) {
    return nodeManager.level(castToChained(bdd).getChainEndVariable());
  }

  private BigInteger selfValue(int level, int chainEndLevel, int childLevel) {
    int n = childLevel - level;
    int m = childLevel - chainEndLevel;

    return BigInteger.valueOf(2).pow(n).subtract(BigInteger.valueOf(2).pow(m - 1));
  }
  /** {@inheritDoc} */
  @Override
  public DD anySat(DD bdd) {
    if (bdd.isLeaf()) {
      return bdd;
    }
    if (bdd.getLow().isFalse()) {
      return nodeManager.makeNode(
          nodeManager.getFalse(),
          anySat(nodeManager.getHigh(bdd)),
          BitMaskIntTupleUtils.createTuple(
              bdd.getVariable(), castToChained(bdd).getChainEndVariable()));
    } else {
      return nodeManager.makeNode(
          anySat(nodeManager.getLow(bdd)),
          nodeManager.getFalse(),
          BitMaskIntTupleUtils.createTuple(
              bdd.getVariable(), castToChained(bdd).getChainEndVariable()));
    }
  }

  private CDD castToChained(DD bdd) {
    Preconditions.checkArgument(bdd instanceof CDD);
    return (CDD) bdd;
  }
}

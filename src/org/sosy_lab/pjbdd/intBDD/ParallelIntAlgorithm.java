// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.ForkJoinWorkerThread;
import org.sosy_lab.pjbdd.api.BDDCreatorBuilder;
import org.sosy_lab.pjbdd.intBDD.cache.IntNotCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntOpCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntQuantCache;

/**
 * Concurrent {@link IntAlgorithm} implementation. Uses concurrent algorithms for bdd node
 * operations.
 *
 * @author Stephan Holzner
 * @see IntBDDAlgorithm
 * @see IntAlgorithm
 * @since 1.0
 */
public class ParallelIntAlgorithm extends IntBDDAlgorithm {

  private final ForkJoinPool pool;

  /**
   * Creates a new {@link ParallelIntAlgorithm} with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDCreatorBuilder} for instantiation
   *
   * @param iteCache - the ite cache
   * @param opCache - the op cache
   * @param notCache - the not cache
   * @param nodeManager - the variable manager
   */
  public ParallelIntAlgorithm(
      IntOpCache opCache,
      IntOpCache iteCache,
      IntNotCache notCache,
      IntQuantCache quantCache,
      IntOpCache composeCache,
      IntNodeManager nodeManager,
      int threads) {
    super(opCache, iteCache, notCache, quantCache, composeCache, nodeManager);
    pool = new ForkJoinPool(threads, new Factory(), null, false);
  }

  /** {@inheritDoc} */
  @Override
  public int makeOp(int f1, int f2, ApplyOp op) {
    return pool.submit(new FJTask(f1, f2, op)).join();
  }

  @Override
  protected int expandExquant(int fLow, int fHigh, int[] f2, int var) {
    ForkJoinTask<Integer> high = pool.submit(() -> makeExquant(fHigh, f2));
    ForkJoinTask<Integer> low = pool.submit(() -> makeExquant(fLow, f2));
    return makeNode(low.join(), high.join(), var);
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    super.shutDown();
    pool.shutdown();
  }

  public class FJTask extends ForkJoinTask<Integer> {

    private static final long serialVersionUID = 1873627492814886644L;
    private final ApplyOp op;

    private final int arg1;
    private final int arg2;
    private int res;

    public FJTask(int f1, int f2, ApplyOp op) {
      nodeManager.addRefs(f1, f2);
      this.arg1 = f1;
      this.arg2 = f2;
      this.op = op;
    }

    @Override
    public Integer getRawResult() {
      return res;
    }

    @Override
    protected void setRawResult(Integer value) {
      this.res = value;
    }

    @Override
    protected boolean exec() {
      if (Thread.currentThread() instanceof ApplyWorker) {
        res = applyCheck(arg1, arg2, op);
        if (res != -1) {
          return true;
        }
        res = apply(arg1, arg2);
        return true;
      }
      return false;
    }

    private int apply(int f1, int f2) {
      int topVar = topVarForLevels(level(f1), level(f2));
      int lowF1 = (level(f1) <= level(f2)) ? low(f1) : f1;
      int lowF2 = (level(f2) <= level(f1)) ? low(f2) : f2;
      int highF1 = (level(f1) <= level(f2)) ? high(f1) : f1;
      int highF2 = (level(f2) <= level(f1)) ? high(f2) : f2;
      applyCheck(lowF1, lowF2, op);
      int high = applyCheck(highF1, highF2, op);
      int low = applyCheck(lowF1, lowF2, op);

      // increase reference count when creation and remove on first use to avoid gbc while resize
      // and node not yet in use
      if (low == -1 && high == -1) {
        ForkJoinTask<Integer> task = new FJTask(lowF1, lowF2, op).fork();
        high = apply(highF1, highF2);
        low = task.join();
      }

      if (low == -1) {
        low = apply(lowF1, lowF2);
      }

      if (high == -1) {
        high = apply(highF1, highF2);
      }

      // push(low, high);
      res = makeNode(low, high, topVar);
      // increase reference count when creation and remove on first use to avoid gbc while resize
      // and node not yet in use
      // pop();
      cacheApply(f1, f2, op, res);
      return res;
    }
  }

  public static class ApplyWorker extends ForkJoinWorkerThread {

    /**
     * Creates a ForkJoinWorkerThread operating in the given pool.
     *
     * @param pool the pool this thread works in
     * @throws NullPointerException if pool is null
     */
    protected ApplyWorker(ForkJoinPool pool) {
      super(pool);
    }
  }

  static class Factory implements ForkJoinPool.ForkJoinWorkerThreadFactory {

    @Override
    public ForkJoinWorkerThread newThread(ForkJoinPool fjPool) {
      return new ApplyWorker(fjPool);
    }
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD.cache;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link IntNotCache} implementation with underlying arrays.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public class IntNotCache {

  /** the backing array used for caching. */
  protected final NotCacheData[] table;

  /**
   * Creates new Cache instances.
   *
   * @param size - the cache size
   */
  public IntNotCache(int size) {
    size = PrimeUtils.getGreaterNextPrime(size);
    table = new NotCacheData[size];
  }

  /**
   * Lookup a entry for given hash.
   *
   * @param hash - the given hash
   * @return found entry or -1
   */
  public NotCacheData get(int hash) {
    int index = Math.abs(hash % table.length);
    return table[index];
  }

  /**
   * Put a computed not operation in cache.
   *
   * @param input - the input value
   * @param res - the resulting negation
   * @return old value if hash collision occurs else -1
   */
  public NotCacheData put(int input, int res) {
    int index = Math.abs(input % table.length);
    NotCacheData old = get(index);
    table[index] = new NotCacheData(input, res);
    return old;
  }

  /** Clear all entries. */
  public void clear() {
    IntStream.range(0, table.length).forEach(i -> table[i] = null);
  }

  public int size() {
    return table.length;
  }

  public int nodeCount() {
    return (int) Arrays.stream(table).filter(Objects::nonNull).count();
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD.cache;

import org.sosy_lab.pjbdd.util.IntArrayUtils;

public class QuantCacheData {
  /** cached input (a,b,c) and result(res) values. */
  private final int input;

  private final int[] levels;
  private final int res;

  /**
   * Creates new IntITEData with given input and result values.
   *
   * @param input - the input bdd value
   * @param levels - the input levels
   * @param res - result value
   */
  QuantCacheData(int input, int[] levels, int res) {
    this.input = input;
    this.levels = levels;
    this.res = res;
  }

  public int result() {
    return res;
  }

  public int f1() {
    return input;
  }

  public int[] levels() {
    return levels.clone();
  }

  public boolean matches(int node, int... mValues) {
    return input == node && IntArrayUtils.equals(levels, mValues);
  }
}

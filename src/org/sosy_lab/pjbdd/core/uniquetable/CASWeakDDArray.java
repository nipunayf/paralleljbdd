// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.lang.ref.WeakReference;
import org.sosy_lab.pjbdd.api.DD;

public class CASWeakDDArray<V extends DD> extends WeakDDArray<V> {

  protected static final VarHandle BDD_HANDLE =
      MethodHandles.arrayElementVarHandle(WeakReference[].class);

  CASWeakDDArray(int size, DD.ChildNodeResolver<V> pResolver) {
    super(size, pResolver);
  }

  @Override
  V get(int i) {
    WeakReference<?> res = (WeakReference<?>) BDD_HANDLE.get(table, i);
    if (res == null) {
      return null;
    }
    return resolver.cast(res.get());
  }
}

// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.uniquetable;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.api.DD;

public class WeakDDArray<V extends DD> {

  protected WeakReference<?>[] table;

  protected final DD.ChildNodeResolver<V> resolver;

  /** {@link ReferenceQueue} for weak references. */
  protected final ReferenceQueue<V> referenceQueue;

  WeakDDArray(int size, DD.ChildNodeResolver<V> pResolver) {
    resolver = pResolver;
    table = new WeakReference<?>[size];
    referenceQueue = new ReferenceQueue<>();
  }

  V get(int i) {
    if (table[i] == null) {
      return null;
    }
    return resolver.cast(table[i].get());
  }

  void set(int i, V value) {
    table[i] = new WeakReference<>(value, referenceQueue);
  }

  Stream<V> toStream() {
    return Stream.of(table)
        .filter(Objects::nonNull)
        .map((ref) -> resolver.cast(ref.get()))
        .filter(Objects::nonNull);
  }

  void resize(int newSize) {
    WeakReference<?>[] newUniqueTable = new WeakReference<?>[newSize];
    System.arraycopy(table, 0, newUniqueTable, 0, table.length);
    table = newUniqueTable;
  }

  public int size() {
    return table.length;
  }

  public int nodeCount() {
    return (int) toStream().count();
  }
}

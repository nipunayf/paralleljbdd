// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.zdd;

import org.sosy_lab.pjbdd.api.DD;

/**
 * Interface for zdd manipulating operations.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public interface ZDDAlgorithm<V extends DD> {

  /**
   * Creates a {@link DD} representing the high subset of an zdd with restricting a given variable
   * to 1.
   *
   * @param zdd - given zdd
   * @param var - {@link DD} representation of given variable
   * @return (subset of zdd with var equals ONE)
   */
  V subSet1(V zdd, V var);

  /**
   * Creates a {@link DD} representing the high subset of an zdd with restricting a given variable
   * to 0.
   *
   * @param zdd - given zdd
   * @param var - {@link DD} representation of given variable
   * @return (subset of zdd with var equals ZERO)
   */
  V subSet0(V zdd, V var);

  /**
   * Creates a {@link DD} with inverted variable.
   *
   * @param zdd - given zdd
   * @param var - {@link DD} representation of given variable
   * @return (invert variable var for zdd)
   */
  V change(V zdd, V var);

  /**
   * Creates a zdd representing the union set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 UNION zdd2)
   */
  V union(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the difference set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 DIFFERENCE zdd2)
   */
  V difference(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the intersection set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 INTERSECT zdd2)
   */
  V intersection(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the product product set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal * } zdd2)
   */
  V product(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the remainder set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal % } zdd2)
   */
  V modulo(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the quotient set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal / } zdd2)
   */
  V division(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the exclusion set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 EXCLUDE zdd2)
   */
  V exclude(V zdd1, V zdd2);

  /**
   * Creates a zdd representing the restriction set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 RESTRICT zdd2)
   */
  V restrict(V zdd1, V zdd2);

  /**
   * Get the empty zdd.
   *
   * @return the zdd false representation.
   */
  V empty();

  /**
   * get the base zdd representation.
   *
   * @return the zdd true representation.
   */
  V base();

  /** Shutdown algorithm instance and components. */
  void shutdown();

  V makeNode(V low, V high, int var);

  /**
   * Enum with zdd operation types. Each type belongs to one binary method.
   *
   * @author Stephan Holzner
   * @version 1.0
   */
  enum ZddOps {
    SUB_SET1,
    SUB_SET0,
    CHANGE,
    UNION,
    DIFF,
    INTSEC,
    MUL,
    MOD,
    EXCLUDE,
    RESTRICT,
    DIV
  }
}

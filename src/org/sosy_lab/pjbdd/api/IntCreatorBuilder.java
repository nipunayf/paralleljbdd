// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.cache.GuavaCache;
import org.sosy_lab.pjbdd.intBDD.CasIntUniqueTable;
import org.sosy_lab.pjbdd.intBDD.IntBDDAlgorithm;
import org.sosy_lab.pjbdd.intBDD.IntBDDNodeManager;
import org.sosy_lab.pjbdd.intBDD.IntCreator;
import org.sosy_lab.pjbdd.intBDD.IntNodeManager;
import org.sosy_lab.pjbdd.intBDD.IntSatAlgorithm;
import org.sosy_lab.pjbdd.intBDD.IntUniqueTable;
import org.sosy_lab.pjbdd.intBDD.IntUniqueTableImpl;
import org.sosy_lab.pjbdd.intBDD.ParallelIntAlgorithm;
import org.sosy_lab.pjbdd.intBDD.SerialIntCreator;
import org.sosy_lab.pjbdd.intBDD.cache.CASIntNotCache;
import org.sosy_lab.pjbdd.intBDD.cache.CASIntOpCache;
import org.sosy_lab.pjbdd.intBDD.cache.CASIntQuantCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntNotCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntOpCache;
import org.sosy_lab.pjbdd.intBDD.cache.IntQuantCache;

/**
 * Builder class to create {@link Creator} environments implemented with {@link IntCreator}.
 *
 * @author Stephan Holzner
 * @see Creator
 * @see IntCreator
 * @since 1.0
 */
public class IntCreatorBuilder extends AbstractCreatorBuilder {

  private boolean disableGc = false;

  /**
   * Creates a new Creator environment with previously set parameters and backing int based
   * algorithms.
   *
   * @return a new int based BDD environment
   */
  @Override
  public Creator build() {
    initParallelismManagerIfNeeded();

    IntUniqueTable uniqueTable =
        useThreadSafeUT
            ? new CasIntUniqueTable(selectedTableSize, selectedIncreaseFactor, disableGc)
            : new IntUniqueTableImpl(selectedTableSize, selectedIncreaseFactor, disableGc);

    IntNodeManager nodeManager = new IntBDDNodeManager(uniqueTable);
    nodeManager.setVarCount(this.selectedVarCount);
    IntOpCache iteCache =
        useThreadSafeUT ? new CASIntOpCache(selectedCacheSize) : new IntOpCache(selectedCacheSize);
    IntOpCache opCache =
        useThreadSafeUT ? new CASIntOpCache(selectedCacheSize) : new IntOpCache(selectedCacheSize);
    IntOpCache composeCache =
        useThreadSafeUT ? new CASIntOpCache(selectedCacheSize) : new IntOpCache(selectedCacheSize);
    IntNotCache notCache =
        useThreadSafeUT
            ? new CASIntNotCache(selectedCacheSize)
            : new IntNotCache(selectedCacheSize);
    IntQuantCache quantCache =
        useThreadSafeUT
            ? new CASIntQuantCache(selectedCacheSize)
            : new IntQuantCache(selectedCacheSize);
    Cache<Integer, BigInteger> satCountCache = new GuavaCache<>();
    satCountCache.init(selectedCacheSize, selectedParallelism);
    IntSatAlgorithm satAlgorithm = new IntSatAlgorithm(nodeManager, satCountCache);
    Creator creator =
        this.selectedThreads > 0 && useThreadSafeUT
            ? new IntCreator(
                new ParallelIntAlgorithm(
                    opCache,
                    iteCache,
                    notCache,
                    quantCache,
                    composeCache,
                    nodeManager,
                    selectedThreads),
                satAlgorithm,
                nodeManager)
            : new SerialIntCreator(
                new IntBDDAlgorithm(
                    opCache, iteCache, notCache, quantCache, composeCache, nodeManager),
                satAlgorithm,
                nodeManager);
    if (synchronizeReorderingOperations) {
      creator = new SynchronizedReorderingCreator(creator);
    }
    return creator;
  }

  public IntCreatorBuilder disableGC() {
    this.disableGc = true;
    return this;
  }
}

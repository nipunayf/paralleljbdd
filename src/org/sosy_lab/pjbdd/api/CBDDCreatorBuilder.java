// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.bdd.BDDCreator;
import org.sosy_lab.pjbdd.bdd.BDDReductionRule;
import org.sosy_lab.pjbdd.cbdd.CBDDApplyAlgorithm;
import org.sosy_lab.pjbdd.cbdd.CBDDForkJoinApplyAlgorithm;
import org.sosy_lab.pjbdd.cbdd.CBDDSat;
import org.sosy_lab.pjbdd.core.algorithm.DDAlgorithm;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.CASArrayCache;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.cache.GuavaCache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.core.node.NodeManagerImpl;

public class CBDDCreatorBuilder extends BDDCreatorBuilder {

  public CBDDCreatorBuilder(DD.Factory<DD> factory) {
    super(factory);
  }

  @Override
  public Creator build() {
    initParallelismManagerIfNeeded();

    NodeManager<DD> nodeManager = new NodeManagerImpl<>(makeTable(), new BDDReductionRule<>());
    nodeManager.setVarCount(this.selectedVarCount);

    Cache<Integer, Cache.CacheData> cache = new CASArrayCache<>();
    Cache<DD, BigInteger> satCache = new GuavaCache<>();
    cache.init(this.selectedCacheSize, this.selectedParallelism);
    satCache.init(this.selectedCacheSize, this.selectedParallelism);

    SatAlgorithm<DD> satAlgorithm = new CBDDSat(satCache, nodeManager);

    DDAlgorithm<DD> algorithm;

    if (parallelizationType == Builders.ParallelizationType.FORK_JOIN) {
      algorithm = new CBDDForkJoinApplyAlgorithm(cache, nodeManager, parallelismManager);
    } else {
      algorithm = new CBDDApplyAlgorithm(cache, nodeManager);
    }
    return new BDDCreator(nodeManager, algorithm, satAlgorithm);
  }
}

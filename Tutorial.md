<!--
This file is part of PJBDD,
a framework for decision diagrams:
https://gitlab.com/sosy-lab/software/paralleljbdd

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# PJBDD Tutorial
This part explains the basic DD operations. It assumes however that you are familiar with BDDs & co. 
## BDDs 

### Create a BDD environment
The first thing to do is creating a Creator object, which will manage all BDD operations.
To initialize Creators, the API entry point `Builders` can be used.
```Java
//instantiate a new bdd creator with 4 worker
Creator creator = Builders.bddBuilder()
              .setVarCount(5)
              .setThreads(4)
              .build();
```
### Creating variables        
Before performing operations variables are needed.

```Java
//Allocate new variables.      
DD f1 = creator.makeVariable();
DD f2 = creator.makeVariable();
//Get specific variables.
DD f3 = creator.makeIthVar(3);
```
Also, there are two special BDD variables that you do not need to allocate. 
These two are the boolean TRUE and FALSE. 
```Java
DD one = creator.makeTrue();
DD zero = creator.makeFalse();
```
### Basic BDD operations
All existing BDD nodes are immutable. 
Therefore all BDD operations return new BDD objects.

BDD operations can be applied by simply calling the corresponding function in the creator:
```Java
DD f1OrF2 = creator.makeOr(f1,f2);
DD f1XorF2 = creator.makeXor(f1,f2);
DD f1AndF2 = creator.makeAnd(f1,f2);
DD f1NandF2 = creator.makeNand(f1,f2);
DD notF1 = creator.makeNot(f1);
DD ifF1ThenF2ElseF3 = creator.makeIte(f1,f2,f3);
```
### Existential Qunatification
To use the existential quantifier (exists - ∃), PJBDD offers two functions:
- `makeExists(DD f1, DD f2)` ∃ f2 . f1
- `makeExists(DD f1, DD[] fs)`∃ cubeOf(fs) . f1

```Java
DD f1ExistF2 = creator.makeExists(f1,f2);
DD f1ExistF2F3 = creator.makeExists(f1,new DD[]{f1,f2});
```
### Variable substitution
It is sometimes desired to substitute variables in a BDD. 
E.g. one could substitute variable `f2` with `f3` in tree `f1AndF2`:
```Java
DD f1AndF3 = creator.makeReplace(f1AndF2, f2, f3);
```
### Composition
Sometimes all occurrences of a variable in a tree needs to be replaced with a formula.
E.g. one could substitute variable `2` with `f1AndF3` in tree `f1AndF2`:

```Java
DD composedBDD = creator.makeCompose(f1AndF2, 2 ,f1AndF3);
```
### Chained BDDs
If you want to use chained BDDs[1], 
you can simply create a CBDD environment which uses the regular BDD API.
But all computations will then internally use chained nodes.
```Java
//instantiate a new cbdd creator with 4 workers
Creator creator = Builders.cbddBuilder()
              .setVarCount(5)
              .setThreads(4)
              .build();
```
## ZDDs 
This part demonstrates basic ZDD[2] operations.
Due to the theoretical difference PJBDD offers a separat ZDD API 

### Create a ZDD environment
The first thing to do is creating a ZDDCreator object, analogously to BDD.
To initialize Creators, the API entry point `Builders` can be used again.
```Java
//instantiate a new bdd creator with 4 worker
ZDDCreator creator = Builders.zddBuilder()()
              .setVarCount(5)
              .setThreads(4)
              .build();
```
### Creating variables
The ZDD counterparts to TRUE and FALSE are BASE and EMPTY.
```Java
DD base = creator.base();
DD empty = creator.empty();
```
Empty and base can be used to create ZDD variables :
```Java
//Allocate new variables.      
DD f1 = creator.makeNode(empty, base, 1);
DD f2 = creator.makeNode(empty, base, 2);
```

### Basic ZDD operations
All existing ZDD nodes are immutable.
Therefore all ZDD operations return new DD objects.

ZDD operations can be applied by simply calling the following functions on the creator:
```Java
DD subSet1(DD zdd, DD var);
DD subSet0(DD zdd, DD var);
DD change(DD zdd, DD var);
DD union(DD zdd1, DD zdd2);
DD difference(DD zdd1, DD zdd2);
DD intersection(DD zdd1, DD zdd2);


```
### Additional ZDD-Operations
In [3] Minato introduces operations for unate cube set algebra:
```Java
DD product(DD zdd1, DD zdd2);
DD modulo(DD zdd1, DD zdd2);
DD division(DD zdd1, DD zdd2);
DD exclude(DD zdd1, DD zdd2);
DD restrict(DD zdd1, DD zdd2);
```

## More features

### Multithreated computations
PJBDD supports concurrent operations with multiple worker threads.
To use the feature, all you need to do is to define the number of worker threads that PJBDD should use.
The following initialization creates an environment which uses four worker threads for internal computations.
```Java
//instantiate a new bdd creator with 4 worker
Creator creator = Builders.bddBuilder()
              .setThreads(4)
              .build();
```


### Sat operations
To figure out how many possible satisfying assignments a DD has, PJBDD offers `satCount`.
Additionally, there is a feature to find one specific satisfiable path with `anySat`.
```Java
//Get number of all satisfying true assignments
BigInteger satCount = creator.satCount(ifF1ThenF2ElseF3);
//Get one satisfiable path
DD anySatisfiablePath = creator.anySat(ifF1ThenF2ElseF3)
```
### Dumping of DDs 
It might be useful to actually see your DDs.
The best way to visualize a (small) DD is to draw it. 
To do this, PJBDD offers export functionalities to the "graphviz" ".dot" format.

```Java
// Initialize DotExporter
DotExporter exporter = new DotExporter();
// Export BDD to .dot String
String dotString = exporter.bddToString(ifF1ThenF2ElseF3);
// Export BDD to .dot and dumps it to file with given path and name
File dotFile = exporter.export(ifF1ThenF2ElseF3, "PATH", "FILE_NAME");
// Export BDD to .dot and dumps it to file with given path and generated name
File dotFileWithGeneratedName = exporter.export(ifF1ThenF2ElseF3, "PATH");
// Export BDD to .dot and dumps it to file in path './' and generated name
File dotFileWithGeneratedNameInDirectory = exporter.export(ifF1ThenF2ElseF3);
```
### Reference counting
In contrast to other BDD libraries manual reference counting is not needed.

### Dynamic variable reordering
PJBDD does not yet support dynamic variable reordering.

### Disabling multi threating
For single-threaded applications, thread safety can bring unnecessary overhead.
Therefore PJBDD allows to disable all threadsafety mechanisms.
```Java
// Create creator environment without thread safety
Creator creator = Builders.bddBuilder().disableThreadSafety()
              .setThreads(4)
              .build();
```

## References
[1] Bryant, Randal E. "Chain reduction for binary and zero-suppressed decision diagrams." Journal of Automated Reasoning 64.7 (2020): 1361-1391

[2] Minato, Shin-ichi. "Zero-suppressed BDDs and their applications." International Journal on Software Tools for Technology Transfer 3.2 (2001): 156-170.

[3] Minato, Shin-ichi. "Zero-suppressed BDDs for set manipulation in combinatorial problems." Proceedings of the 30th International Design Automation Conference. 1993.
